<?php
// we werken nu lekker veilig met sessies.
session_start();

// request teller... gewoon voor de lol
if (!isset($_SESSION['count'])) {
    $_SESSION['count'] = 0;
  } else {
    $_SESSION['count']++;
  }
  
// Controleert eerst of GET variabelen mee zijn gestuurd
if (isset($_POST['name']) && isset($_POST["password"])) {
    $name = $_POST['name'];
    $password = $_POST["password"];
    // Controleer naam en wachtword
    if ($name=='Doofenshmirz' && $password='1234') {
        // Naam en wachtwoord zijn gelijk!
        $_SESSION["name"] = $name;
    }
} else if (isset($_SESSION["name"])) {
    $name = $_SESSION["name"];    
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Hello Cookie</title>
</head>
<body>
    <?php if (isset($name)) {
        echo "<h1>Hallo $name, welkom bij de World Domination Site</h1>";
        // hieronder komt wat gewone html, dat binnen het if statement hoort.
        ?>
		<form method="get">
			<h1>Wat wilt u doen?</h1>
	  		<input type="radio" name="action" value="zombie" checked>Zombievirus loslaten</input><br>
  			<input type="radio" name="action" value="meltinator">Smelt de poolkappen met de Smeltinator</input><br>
  			<input type="radio" name="action" value="puppy">Aai een puppy</input><br/>
			<input type="submit" value="Verzenden">
		</form>        
    <?php } else { // nu het 'else' deel van het if-statement ?>
    <h1>Type je naam in het veld en klik 'Submit'</h1>
    <form method="post">
        <input type="text" name="name" value=""><br>
        <input type="password" name="password" value=""><br>
        <input type="submit" value="Inloggen">
    </form>
    <?php }  // einde van het else deel
    // laat het aantal requests in deze sessie zien
    if (isset($_SESSION["count"])) {
        echo "<p>Aantal requests=".$_SESSION["count"];
    }
    ?>
</body>
</html>