<?php
if (isset($_GET['name'])) {
    $name = $_GET['name'];
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Hello Form</title>
</head>
<body>
    <?php if (isset($name)) {
        echo "<h1>Hallo $name</h1>";
    } ?>
    <h1>Type je naam in het veld en klik 'Submit'</h1>
    <form action="hello-form.php" method="get">
        <input type="text" name="name" value="">
        <br><br>
        <input type="submit" value="Verzenden">
    </form>
</body>
</html>